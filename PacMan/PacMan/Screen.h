#pragma once

#include "Game.h"

using namespace sf;

class Screen {
protected:
	Screen() {}
public:
	virtual ~Screen() {}

	virtual void update(Event event) = 0;
	virtual void render(Time elapsedTime) = 0;
};