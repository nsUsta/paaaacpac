#pragma once
#include "FenetreGrapheSFML.h"

class FRepereMonde : public FenetreGrapheSFML {

public:
	FRepereMonde(const string & titre, unsigned int fondCarte,
		const Vecteur2D & coinBG, const Vecteur2D & coinHD,
		const int largeur, const int hauteur, const Font & font) : FenetreGrapheSFML(titre, fondCarte, coinBG, coinHD, largeur, hauteur, font){

		Vecteur2D C, D(1, 0), E(0, 1);
		Vecteur2D A1(coinBG.x, 0), A2(coinBG.x, 0);
		Vecteur2D B1(coinBG.x, 0), B2(coinBG.x, 0);


		C = t.applique(C);
		D = t.applique(D);
		E = t.applique(E);

		A1 = t.applique(A1);
		A2 = t.applique(A2);

		B1 = t.applique(B1);
		B2 = t.applique(B2);



		VertexArray abscisses(PrimitiveType::Lines);
		abscisses.append(Vertex(vecteur2DToVector2f(A1), Color::Black));
		abscisses.append(Vertex(vecteur2DToVector2f(A2), Color::Black));

		fenetre.draw(abscisses);


		float rayon = 4;
		Vecteur2D u(1, 1);
		Vecteur2D ru = u * rayon;

		CircleShape F(rayon);
		F.setFillColor(Color::Blue);
		F.setPosition(vecteur2DToVector2f(C - ru));
		fenetre.draw(F);

		F.setPosition(vecteur2DToVector2f(D - ru));
		fenetre.draw(F);

		F.setPosition(vecteur2DToVector2f(E - ru));
		fenetre.draw(F);

	}

	~FRepereMonde(){}
};