#pragma once

#include "FRepereMonde.h"
#include "Vecteur2D.h"
#include "Screen.h"

#include "SFML/Graphics.hpp"
#include "SFML/Graphics/Color.hpp"

using namespace sf;

class Game {
private:
	unsigned int jaune0 = 0xDFFF00FF;   // jaune opaque
	FRepereMonde * _window;
	Screen * _screen;
	Font f;

public:
	Game() {

		_window = new FRepereMonde("Pac-Man", 0xDFFF00FF, Vecteur2D(-1, -1), Vecteur2D(9, 9), 800, 500, f);
	}


	virtual ~Game() {
		delete _window;
		delete _screen;

		_window = nullptr;
		_screen = nullptr;
	}

	void run() {
		Clock clock;
		while (_window->fenetre.isOpen()) {
			Event event;
			Time elapsedTime = clock.restart();
			while (_window->fenetre.pollEvent(event)) {
				_screen->update(event);

				if (event.type == Event::EventType::Closed)
					_window->fenetre.close();
			}

			_window->fenetre.clear(Color::Black);
			_screen->render(elapsedTime);
			_window->fenetre.display();
		}
	}

	Screen * getScreen() const { return _screen; }

	void setScreen(Screen * newScreen) { _screen = newScreen; }


};