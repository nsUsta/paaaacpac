#pragma once

#include "GameElementMove.h"
class Fantome : public GameElementMove {
public:
	/**
	 *  \brief Constructeur
	 *
	 *  Constructeur de la classe GameElementMove
	 */
	Fantome() :GameElementMove("fantome", 1) {}
	virtual ~Fantome() {}
};