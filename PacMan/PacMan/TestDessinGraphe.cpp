/*

Test de l'op�ration dessiner un graphe planaire

L'info associ�e aux sommets est un VSommet (nom, position, couleur)  (par exemple)
L'info associ�e aux ar�tes est une combinaison de 2 couleurs : une de fond et une superficielle plus ou moins transparente (par exemple)

*/
#include <iostream>
#include <sstream>
#include <string>

#include <SFML\Graphics\Color.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/VertexArray.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Mouse.hpp>
#include <SFML/Graphics/Font.hpp>

#include "Graphe.h"
#include "FenetreGrapheSFML.h"
#include "InfosGrapheDessin.h"

using namespace std;
using namespace sf;



int main()
{
	/* chargement des polices de caract�res */

	Font font1, font2;
	bool ok;

	char ch;

	Graphe<Peinture, VSommet> g1;	// cr�ation � vide

	Sommet<VSommet> *s;
	vector<Sommet<VSommet>*> sommets;
	Arete<Peinture, VSommet> *a;
	vector<Arete<Peinture, VSommet>*> arretes;



	//--------------------- Couleurs --------------------------
	unsigned int magenta = Color::Magenta.toInteger();		// r�cup�re la couleur Magenta au format nombre entier non sign�
	unsigned int jaune0 = 0xDFFF00FF;   // jaune opaque
	unsigned int jaune1 = 0xDFFF00C0;   // jaune presque opaque
	unsigned int jaune2 = 0xDFFF0064;   // jaune un peu transparent
	unsigned int jaune3 = 0xDFFF0020;	// jaune presque transparent

	unsigned int tangerine0 = 0xFF7F00FF; // tangerine opaque
	unsigned int tangerine1 = 0xFF7F00DF; // tangerine presque opaque
	unsigned int tangerine2 = 0xFF7F00BF; // tangerine un peu transparent
	unsigned int tangerine3 = 0xFF7F0060; // tangerine presque transparent
	unsigned int tangerine4 = 0xFF7F0000; // tangerine  transparent

	unsigned int turquoise = 0x00CED1FF;	// couleur turquoise opaque. pr�fixe 0x pour h�xad�cimal. format RGBA
	unsigned int vertCitron = 0x00FF00FF;


	// grille de 7*7
	for (int i = 0; i < 7; i++) {
		for (int j = 0; j < 7; j++) {
			s = g1.creeSommet(VSommet("", Vecteur2D(i, j), turquoise));
			sommets.push_back(s);
		}
	}
	//----------------- on connecte certains sommets -------------------

	Arete<Peinture, VSommet> * a0, *a1, *a2, *a3, *a4;
	for (int i = 8; i < 49; i++) {
		a = g1.creeArete(Peinture(vertCitron, tangerine0), sommets[i], sommets[i % 7]);
		arretes.push_back(a);
	}
	for (int i = 0; i < 48; i++) {
		if ((i % 7) != 6) a = g1.creeArete(Peinture(vertCitron, tangerine0), sommets[i], sommets[i + 1]);
		arretes.push_back(a);
	}

	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[0], sommets[8]));

	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[1], sommets[9]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[7], sommets[15]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[8], sommets[16]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[9], sommets[17]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[15], sommets[23]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[16], sommets[24]));


	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[11], sommets[17]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[12], sommets[18]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[13], sommets[19]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[5], sommets[11]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[6], sommets[12]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[19], sommets[25]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[18], sommets[24]));

	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[23], sommets[29]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[24], sommets[30]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[24], sommets[32]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[25], sommets[33]));

	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[26], sommets[34]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[33], sommets[41]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[32], sommets[40]));

	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[31], sommets[39]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[39], sommets[47]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[31], sommets[37]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[37], sommets[43]));


	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[30], sommets[36]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[36], sommets[42]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[29], sommets[35]));
	arretes.push_back(g1.creeArete(Peinture(vertCitron, tangerine0), sommets[40], sommets[48]));

	//========== Pacman
	VSommet pacman = VSommet("Pacman", Vecteur2D(0, 0), jaune0);
	sommets[0]->v = pacman;
	Vecteur2D place;
	int tempplace = 0;
	//------------------ on dessine le graphe ----------------

	cout << "le graphe cr�� g1 est :" << endl << g1 << endl;

	string titre("PACMAN");
	int largeur = 700, hauteur = 700;
	//Vecteur2D  coinBG(0,hauteur), coinHD(largeur,0);

	Vecteur2D  coinBG(-0.5, -0.5), coinHD(6.5, 6.5);

	unsigned int fondCarte = 0xEFEFEFFF;	// sorte de gris clair ~= �tain pur

	FenetreGrapheSFML fenetreGraphe(titre, fondCarte, coinBG, coinHD, largeur, hauteur, font1);

	ok = g1.dessine(fenetreGraphe);
	cout << "graphe dessin� ? " << (ok ? "succ�s" : "�chec") << endl;
	fenetreGraphe.fenetre.display();

	//boucle infinie de contr�le des �v�nements de la fen�tre
	while (fenetreGraphe.fenetre.isOpen())
	{
		Event event;
		while (fenetreGraphe.fenetre.pollEvent(event))
		{
			switch (event.type)
			{
			case Event::Closed:
				fenetreGraphe.fenetre.close();
			case Event::KeyPressed:
				switch (event.key.code)
				{
				case Keyboard::Numpad1:
					cout << "Numpad1" << endl;
					place = Vecteur2D(-1, -1);
					tempplace = -8;
					break;
				case Keyboard::Numpad2:
					cout << "Numpad2" << endl;
					place = Vecteur2D(0, -1);
					tempplace = -1;
					break;
				case Keyboard::Numpad3:
					cout << "Numpad3" << endl;
					place = Vecteur2D(1, -1);
					tempplace = 6;
					break;
				case Keyboard::Numpad4:
					cout << "Numpad4" << endl;
					tempplace = -7;
					break;
				case Keyboard::Numpad5:
					cout << "Numpad5" << endl;
					place = Vecteur2D(0, 0);
					tempplace = 0;
					break;
				case Keyboard::Numpad6:
					cout << "Numpad6" << endl;
					place = Vecteur2D(1, 0);
					tempplace = 7;
					break;
				case Keyboard::Numpad7:
					cout << "Numpad7" << endl;
					place = Vecteur2D(-1, 1);
					tempplace = -6;
					break;
				case Keyboard::Numpad8:
					cout << "Numpad8" << endl;
					place = Vecteur2D(0, 1);
					tempplace = 1;
					break;
				case Keyboard::Numpad9:
					cout << "Numpad9" << endl;
					place = Vecteur2D(1, 1);
					tempplace = 8;
					break;
				default:
					break;
				}

				pacman.p = pacman.p + place;
				
				g1.dessine(fenetreGraphe);

				fenetreGraphe.fenetre.display();
				/*
				cout << "Touche press�e, place :" << place << endl;
				sommets[place]->EchangerValeur(sommets[place +tempplace]);
				place += tempplace;*/
				break;
			default:
				break;
			}
		}


	}
	return 0;
}
