#pragma once

#include <string>
#include "Vecteur2D.h"
#include "Erreur.h"
class GameElement {
	Vecteur2D _pos;
	string _nom;
protected:
	GameElement(const string & nom){
		if (nom == "") throw Erreur("Nom vide");
		_nom = nom;
		_pos = Vecteur2D(1, 3);
	}

public:

	virtual ~GameElement(){}
	
	void setPosition(Vecteur2D p) { _pos = p; }
	void setPosition(float x, float y) { _pos = Vecteur2D(x, y); }

	const string & getName() const { return _nom; }
	Vecteur2D getPosition()const { return _pos; }
};