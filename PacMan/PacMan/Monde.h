#pragma once

#include "Pacman.h"
#include "Fantome.h"

class Monde {
	Pacman * _p;
	Fantome * _f;
public:

	Monde() {
		_p = new Pacman();
		_f = new Fantome();
	}

	~Monde() {
		delete _p;
		delete _f;
		_p = nullptr;
		_f = nullptr;
	}

	Pacman * getPacman() const { return _p;}
	Fantome * getFantome()const { return _f;}
};