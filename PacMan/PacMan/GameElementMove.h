#pragma once

#include "GameElement.h"

class GameElementMove : public GameElement {
	int _vitesse;
protected:
	GameElementMove(const string & nom, int vitesse): GameElement(nom), _vitesse(vitesse){}

public:
	void move(Vecteur2D deplacement) { setPosition(this->getPosition() + (deplacement*_vitesse)); }

	virtual ~GameElementMove(){}
};